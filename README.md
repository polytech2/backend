# Инструкция по запуску
## Для запуска на новом сервере необходимо выполнить последовательно:
* sudo snap install docker
* sudo git clone https://gitlab.com/polytech2/backend.git
* cd backend/docker
* sudo docker-compose build
* sudo docker-compose up -d