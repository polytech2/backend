const router = require('express').Router()
module.exports = router;

const connection    = require('../postgres/my_sql_wrapper');
const db_api        = require('../postgres/api_v1_db_helper')(connection);
const db_quiz       = require('../postgres/quiz_db_helper')(connection);


router.get('/users/telegram/:id', async (req, res) => {
    const data = { telegram_id: req.params.id }
    let [error, results] = await db_api.findUserByTelegramID(data);
    if (error != null) return failure(res, error)
    success(res, results);
});


router.get('/groups/:group_number', async (req, res) => {
    const data = { group_number: req.params.group_number }
    let [error, results] = await db_api.getGroups(data);
    if (error != null) return failure(res, error)
    success(res, results);
});

router.get('/groups/:group_number_1/:group_number_2', async (req, res) => {
    const data = { group_number: req.params.group_number_1 + '/' + req.params.group_number_2 }
    let [error, results] = await db_api.getGroups(data);
    if (error != null) return failure(res, error)
    success(res, results);
});


router.patch('/teachers/:teacher_id', async (req, res) => {
    let data = req.body; data.id = req.params.teacher_id
    let [error, results] = await db_api.updateTelegramIdForTeacher(data);
    if (error != null) return failure(res, error)
    success(res, results);
})

router.get('/teachers/:name_pattern', async (req, res) => {
    const data = { name_pattern: req.params.name_pattern }
    let [error, results] = await db_api.getTeachersByInitials(data)
    if (error != null) return failure(res, error)
    success(res, results);
})

router.get('/teachers/:teacher_id/lessons', async (req, res) => {
    const data = { teacher_id: req.params.teacher_id }
    let [error, results] = await db_api.getAllLessonsForTeacher(data)
    if (error != null) return failure(res, error)
    success(res, results);
})

router.post('/teachers/:teacher_id/binding', async (req, res) => {
    let data = req.body; data.id = req.params.teacher_id
    let [error, results] = await db_api.bindTeacherWithLessonAndGroup(data);
    if (error != null) return failure(res, error)
    success(res, results);
})

router.delete('/teachers/:teacher_id/binding', async (req, res) => {
    let data = req.body; data.id = req.params.teacher_id
    let [error, results] = await db_api.unbindTeacherWithLessonAndGroup(data);
    if (error != null) return failure(res, error)
    success(res, results);
})

router.get('/teachers/:teacher_id/binding', async (req, res) => {
    const data = { teacher_id: req.params.teacher_id }
    let [error, results] = await db_api.getBindingsForTeacher(data)
    if (error != null) return failure(res, error)
    success(res, results);
})


router.get('/students', async (req, res) => {
    let [error, results] = await db_api.getStudents();
    if (error != null) return failure(res, error)
    success(res, results);
});

router.post('/students', async (req, res) => {
    let [error, results] = await db_api.addStudent(req.body);
    if (error != null) return failure(res, error)
    success(res, results);
});

router.delete('/students/:id', async (req, res) => {
    const data = { id: req.params.id }
    let [error, results] = await db_api.deleteStudent(data);
    if (error != null) return failure(res, error)
    success(res, results);
});


router.get('/quiz/:lesson_id', async (req, res) => {
    const data = {lessons_on_day_id: req.params.lesson_id}
    let [error, results] = await db_quiz.getAllQuestionsOnLesson(data);
    if (error != null) return failure(res, error)
    success(res, results);
});

router.post('/quiz/:lesson_id', async (req, res) => {
    let data = { lessons_on_day_id: req.params.lesson_id }
    let await_r, answer_type = req.body.answer_type

    if (answer_type === 'numeric') {
        data.answer_type = answer_type
        data.answer = Math.min(10, parseInt(req.body.answer))
        data.question_id = req.body.question_id
        data.student_id = req.body.student_id

        await_r = await db_quiz.addNumericAnswer(data);
    } else if (answer_type === 'text') {
        data.answer_type = answer_type
        data.answer = req.body.answer.trim()
        data.question_id = req.body.question_id
        data.student_id = req.body.student_id

        await_r = await db_quiz.addTextAnswer(data);
    } else if (answer_type === 'questions')
    {
        const questions = req.body.answer.split("<:sp:>")
        for (const question of questions)
        {
            const data_clone = JSON.parse(JSON.stringify(data));
            data_clone.text = question.trim()

            await_r = await db_quiz.addQuestion(data_clone);
            if (await_r.error != null) break;
        }
    }

    if (await_r.error != null) return failure(res, await_r.error)
    success(res, await_r.results);
});

router.get('/quiz/:lesson_id/answers', async (req, res) => {
    const data = { lessons_on_day_id: req.params.lesson_id }
    let [error, results] = await db_quiz.getAllAnswersOnLesson(data);
    if (error != null) return failure(res, error)
    success(res, results);
});


function success(res, message, code = 200) {
    res.status(code).json(message);
}

function failure(res, sql_error, code = 400) {
    const error = {error: sql_error.detail ? sql_error.detail : sql_error};
    res.status(code).send(JSON.stringify(error));
}
