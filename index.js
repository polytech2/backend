const portHttp = 10080;
const portHttps = 10443;

const express = require('express');
const swaggerUi = require('swagger-ui-express');
const http = require('http');
const https = require('https');
const fs = require('fs');
const cors = require('cors');
const cluster = require('cluster');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const path = require('path');
const router = express.Router();
global.fetch = require("node-fetch");

const mySqlConnection = require('./postgres/my_sql_wrapper');
const quizDBHelper = require('./postgres/quiz_db_helper')(mySqlConnection);
const tableDBHelper = require('./postgres/table_db_helper')(mySqlConnection);
const polytechTableCacher = require('./node/polytech_table_cacher')(tableDBHelper);
const polytechNotificationManager = require('./node/polytech_notification_manager')(tableDBHelper,quizDBHelper);
const croneExecutor = require('./node/crone_executor')(polytechTableCacher,polytechNotificationManager);
const swaggerDocument = require('./api/swagger.json');

const expressApp = express();
module.exports = expressApp; // for testing

expressApp.use(bodyParser.urlencoded({extended: true}));
expressApp.use(bodyParser.json());
expressApp.use(logger('dev'));
expressApp.use(cookieParser());
expressApp.use(cors());
expressApp.use(helmet());

expressApp.use('/', router);

router.use('/api/v1', swaggerUi.serve);
router.get('/api/v1', swaggerUi.setup(swaggerDocument));
router.use('/api/v1', require('./api/rest_api_v1'))

expressApp.set("view engine", "pug");
expressApp.set("views", path.join(__dirname, "views"));
router.get('/quiz/:lesson_id', async(req, res) => {
    const data = {lessons_on_day_id: req.params.lesson_id}
    let [error, results] = await quizDBHelper.getAllQuestionsOnLesson(data);
    if (error !== null) {
        res.status(404).send(error);
    }
    else {
        res.render("web_page", {'data' : results, 'action_url' : req.params.lesson_id});
    }
});

router.post('/quiz/:lesson_id', async(req, res) => {
    for (var key in req.body) {
        const split_data = key.split(' ');
        let json = {
            answer_type : split_data[1],
            answer : req.body[key],
            question_id : split_data[0],
            student_id : null
        }
        let data = { lessons_on_day_id: req.params.lesson_id }

        if (json.answer_type === 'numeric') {
            data.answer_type = json.answer_type
            data.answer = Math.min(10, parseInt(json.answer))
            data.question_id = json.question_id
            data.student_id = json.student_id

            let await_r = await quizDBHelper.addNumericAnswer(data);
            if (await_r.error != null) {
	        return res.status(200).send(await_r.error);
	    }
        } else if (json.answer_type === 'text') {
            data.answer_type = json.answer_type
            data.answer = json.answer.trim()
            data.question_id = json.question_id
            data.student_id = json.student_id

            let await_r = await quizDBHelper.addTextAnswer(data);
            if (await_r.error != null) {
	        return res.status(200).send(await_r.error);
	    }
        } else if (json.answer_type === 'questions')
        {
            const questions = json.answer.split("\r\n")
            for (const question of questions)
            {
                const data_clone = JSON.parse(JSON.stringify(data));
                data_clone.text = question.trim()
                if (data_clone.text !== '') {
                    let await_r = await quizDBHelper.addQuestion(data_clone);
                    if (await_r.error != null) {
	                return res.status(200).send(await_r.error);
		    }
                }
            }
        }
    }
    res.render("web_page_complete");
});


expressApp.use((err, req, res, next) => {
    // This check makes sure this is a JSON parsing issue:
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        res.sendStatus(400); // Bad request
        return;
    } else if (err !== null) {
        console.error(err);
        res.status(400).send('Something broke!');
        return;
    }

    next();
});

const workers = process.env.WORKERS || require('os').cpus().length;
// init the server
if (cluster.isMaster) {

    //INIT DB
    console.log('\x1b[36m%s\x1b[0m', 'Init db...');
    const sqlInit = fs.readFileSync(path.join(__dirname, './postgres/create_schema.sql')).toString();
    mySqlConnection.query(sqlInit, []).then(error => {
        if (error[0] != null) {
            throw "Can't connect to DB"
        } else {
            console.log('\x1b[36m%s\x1b[0m', 'Init db: success');
            croneExecutor.init()
        }

        parseArguments()
    })
    ////////////

    console.log('start cluster with %s workers', workers);

    for (let i = 0; i < workers; ++i) {
        const worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    }

    cluster.on('exit', function (worker) {
        console.log('worker %s stop.', worker.process.pid);
        cluster.fork();
    });
} else {
    http.createServer(expressApp)
        .listen(portHttp, function () {
            console.log('listening http on port ' + portHttp);
        });

    https.createServer({
        key: fs.readFileSync('ssl/server.key'),
        cert: fs.readFileSync('ssl/server.cert')
    }, expressApp)
        .listen(portHttps, function () {
            console.log('listening https on port ' + portHttps);
        });
}

function parseArguments() {
    console.log("Parsing arguments...");
    process.argv.forEach(function (arg, index, array) {
        const key_val = arg.split(new RegExp("[ \t]*=[ \t]*"))
        if (key_val.length !== 2) return console.log(' * invalid arg: ' + arg);

        const key = key_val[0], val = key_val[1]
        console.log(' * ' + key + ': ' + val);

        switch (key) {
            case "init": {
                if (val === 'true') initDB();
                break;
            }
        }
    });
    console.log("Parsing finished");
}

/**
 * Наполняет базу данных значениями, не дожидаясь cron-задачи.
 */
function initDB() {
    polytechTableCacher.download(() => { console.log('\x1b[34m%s\x1b[0m', 'Cron finish'); })
}
