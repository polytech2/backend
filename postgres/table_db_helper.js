let pgConnection

module.exports = injectedPgConnection => {

    pgConnection = injectedPgConnection

    return {
        addDay: insertDay,
        addFaculty: insertFaculty,
        addGroup: insertGroup,
        addLesson: insertLesson,
        addSelectedLessonForTeacher: insertSelectedLessonForTeacher,
        addStudent: insertStudent,
        addTeacher: insertTeacher,
        addTeacherOnLesson: insertTeacherOnLesson,
        addLessonsOnDay: insertLessonsOnDay,
        addGroupsOnLesson: insertGroupsOnLesson,
        clearOldDays: deleteOldDays,
        getAllLessonsOnGroup: selectAllLessonsOnGroup
    }
}

async function insertDay(data) {
    const query = `INSERT INTO days (date)
                   VALUES ($1)
                   ON CONFLICT (date) DO UPDATE SET date = $1
                   RETURNING *;`
    return await pgConnection.query(query, [data.date])
}

async function insertFaculty(data) {
    const query = `INSERT
                   INTO faculties(id, name, abbr)
                   VALUES ($1, $2, $3)
                   ON CONFLICT DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.id, data.name, data.abbr])
}

async function insertGroup(data) {
    const query = `INSERT
                   INTO groups(id, faculty_id, name, spec)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.id, data.faculty_id, data.name, data.spec])
}

async function insertLesson(data) {
    const query = `INSERT
                   INTO lessons(subject, subject_short, type)
                   VALUES ($1, $2, $3)
                   ON CONFLICT
                       ON CONSTRAINT lessons_subject_subject_short_type_key
                       DO UPDATE SET subject       = $1,
                                     subject_short = $2,
                                     type          = $3
                   RETURNING *;`
    return await pgConnection.query(query, [data.subject, data.subject_short, data.type])
}

async function insertLessonsOnDay(data) {
    const query = `INSERT
                   INTO lessons_on_day(lesson_id, day_id, time_start, time_end)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT ON CONSTRAINT lessons_on_day_lesson_id_day_id_time_start_time_end_key DO UPDATE SET lesson_id  = $1,
                                                                                                                   day_id     = $2,
                                                                                                                   time_start = $3,
                                                                                                                   time_end   = $4
                   RETURNING *;`
    return await pgConnection.query(query, [data.lesson_id, data.day_id, data.time_start, data.time_end])
}

async function insertGroupsOnLesson(data) {
    const query = `INSERT
                   INTO groups_on_lesson(lessons_on_day_id, group_id)
                   VALUES ($1, $2)
                   ON CONFLICT ON CONSTRAINT groups_on_lesson_pkey DO UPDATE SET lessons_on_day_id = $1,
                                                                                 group_id          = $2
                   RETURNING *;`
    return await pgConnection.query(query, [data.lessons_on_day_id, data.group_id])
}

async function insertSelectedLessonForTeacher(data) {
    const query = `INSERT
                   INTO selected_lessons_for_teachers(id, teacher_id, lesson_id, group_id)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT (id) DO UPDATE SET id         = $1,
                                                  teacher_id = $2,
                                                  lesson_id  = $3,
                                                  group_id   = $4
                   RETURNING *;`
    return await pgConnection.query(query, [data.id, data.teacher_id, data.lesson_id, data.group_id])
}

async function insertStudent(data) {
    const query = `INSERT
                   INTO students(telegram_id, login, group_id)
                   VALUES ($1, $2, $3)
                   ON CONFLICT ON CONSTRAINT students_telegram_id_key DO UPDATE SET telegram_id = $1,
                                                                                    login       = $2,
                                                                                    group_id    = $3
                   RETURNING *;`
    return await pgConnection.query(query, [data.telegram_id, data.login, data.group_id])
}

async function insertTeacher(data) {
    const query = `INSERT
                   INTO teachers(id, telegram_id, full_name, chair)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT (id) DO UPDATE SET id          = $1,
                                                  telegram_id = $2,
                                                  full_name   = $3,
                                                  chair       = $4
                   RETURNING *;`
    return await pgConnection.query(query, [data.id, data.telegram_id, data.full_name, data.chair])
}

async function insertTeacherOnLesson(data) {
    const query = `INSERT
                   INTO teachers_on_lesson(lessons_on_day_id, teacher_id)
                   VALUES ($1, $2)
                   ON CONFLICT DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.lessons_on_day_id, data.teacher_id])
}

async function deleteOldDays() {
    const query = `DELETE
                   FROM days
                   WHERE date < now() - interval '1 week'
                   RETURNING *;`
    return await pgConnection.query(query, [])
}

async function selectAllLessonsOnGroup(data) {
    const query = `SELECT DISTINCT ON (subject) json_build_object(
                                                        'subject', les.subject,
                                                        'type', les.type,
                                                        'lesson_id', les.id
                                                    )
                   FROM lessons as les
                            JOIN lessons_on_day lod on les.id = lod.lesson_id
                            JOIN groups_on_lesson gol on lod.id = gol.lessons_on_day_id
                   WHERE gol.group_id = $1;`
    return await pgConnection.query(query, [data.group_id])
}
