let pgConnection

module.exports = injectedPgConnection => {

    pgConnection = injectedPgConnection

    return {
        findUserByTelegramID: findUserByTelegramID,
        getTeachersByInitials: getTeachersByInitials,
        updateTelegramIdForTeacher: updateTelegramIdForTeacher,
        getGroups: getGroups,
        addStudent: addStudent,
        deleteStudent: deleteStudent,
        getStudents: getStudents, // just test case
        getAllLessonsForTeacher: getAllLessonsForTeacher,
        getBindingsForTeacher: getBindingsForTeacher,
        bindTeacherWithLessonAndGroup: bindTeacherWithLessonAndGroup,
        unbindTeacherWithLessonAndGroup: unbindTeacherWithLessonAndGroup,
    }
}

async function findUserByTelegramID(data) {
    const query =  `SELECT id, login as name, 'student' as type
                    FROM students
                    WHERE telegram_id = $1
                    UNION
                    SELECT id, full_name as name, 'teacher' as type
                    FROM teachers
                    WHERE telegram_id = $1;`
    return await pgConnection.query(query, [data.telegram_id])
}

async function getTeachersByInitials(data) {
    const query =  `SELECT id, full_name, chair
                    FROM teachers
                    WHERE lower(full_name) LIKE lower($1);`
    return await pgConnection.query(query, [data.name_pattern])
}

async function updateTelegramIdForTeacher(data) {
    let [error, results] = await findUserByTelegramID({telegram_id: data.telegram_id})
    if (error != null) return [error, results]
    else if (results.length > 0 && results[0].type === "student") {
        return ["student with this telegram id already exist", null]
    }

    const query =  `UPDATE teachers
                    SET telegram_id = $2
                    WHERE id = $1;`
    return await pgConnection.query(query, [data.id, data.telegram_id])
}

async function getGroups(data) {
    const query =  `SELECT id, name, spec
                    FROM groups
                    WHERE name LIKE ('%'||$1||'%')`
    return await pgConnection.query(query, [data.group_number])
}

async function addStudent(data) {
    let [error, results] = await findUserByTelegramID({telegram_id: data.telegram_id})
    if (error != null) return [error, results]
    else if (results.length > 0) return ["person with this telegram id already exist", null]

    const query = `INSERT
                   INTO students(telegram_id, login, group_id)
                   VALUES ($1, $2, $3)
                   ON CONFLICT ON CONSTRAINT students_telegram_id_key DO UPDATE SET telegram_id = $1,
                                                                                    login       = $2,
                                                                                    group_id    = $3
                   RETURNING *;`
    return await pgConnection.query(query, [data.telegram_id, data.login, data.group_id])
}

async function deleteStudent(data) {
    const query =  `DELETE
                    FROM students
                    WHERE id = $1`
    return await pgConnection.query(query, [data.id])
}

async function getStudents() {
    const query = `SELECT *
                   FROM students`
    return await pgConnection.query(query)
}

async function getAllLessonsForTeacher(data) {
    const query =  `SELECT id, type, subject, groups
                    FROM lessons
                    CROSS JOIN (
                        SELECT array_agg(json_build_object('id', id, 'name', name)) as groups
                        FROM groups
                        WHERE id IN (
                            SELECT group_id
                            FROM groups_on_lesson
                            WHERE lessons_on_day_id IN (
                                SELECT id
                                FROM lessons_on_day
                                WHERE id IN (
                                    SELECT lessons_on_day_id
                                    FROM teachers_on_lesson
                                    WHERE teacher_id = $1
                                )
                            )
                        )
                    ) as groups
                    WHERE id IN (
                        SELECT lesson_id
                        FROM lessons_on_day
                        WHERE id IN (
                            SELECT lessons_on_day_id
                            FROM teachers_on_lesson
                            WHERE teacher_id = $1)
                        AND day_id IN (
                            SELECT id
                            FROM days
                            WHERE date >= now() - interval '1 week')
                    )`
    return await pgConnection.query(query, [data.teacher_id])
}

async function getBindingsForTeacher(data) {
    const query = `SELECT *
                   FROM selected_lessons_for_teachers
                   WHERE teacher_id = $1`
    return await pgConnection.query(query, [data.teacher_id])
}

async function bindTeacherWithLessonAndGroup(data) {
    const query =  `INSERT
                    INTO selected_lessons_for_teachers(teacher_id, lesson_id, group_id)
                    VALUES ($1, $2, $3)
                    ON CONFLICT ON CONSTRAINT selected_lessons_for_teachers_teacher_id_lesson_id_group_id_key DO NOTHING`
    return await pgConnection.query(query, [data.teacher_id, data.lesson_id, data.group_id])
}

async function unbindTeacherWithLessonAndGroup(data) {
    const query =  `DELETE
                    FROM selected_lessons_for_teachers
                    WHERE teacher_id = $1 AND lesson_id = $2 AND group_id = $3`
    return await pgConnection.query(query, [data.teacher_id, data.lesson_id, data.group_id])
}