let pgConnection

module.exports = injectedPgConnection => {

    pgConnection = injectedPgConnection

    return {
        addQuestion: insertQuestion,
        addTextAnswer: insertTextAnswer,
        addNumericAnswer: insertNumericAnswer,
        getAllAnswersOnLesson: selectAllAnswersOnLesson,
        getAllQuestionsOnLesson: selectAllQuestionsOnLesson,
        getTeachersForNotificationAfterEndQuiz: selectTeachersForNotificationAfterEndQuiz,
        getUsersForNotificationAfterLesson: selectUsersForNotificationAfterLesson,
    }
}

async function insertQuestion(data) {
    const query = `INSERT
                   INTO questions(text, type, lessons_on_day_id)
                   VALUES ($1, 'text'::question_type, $2)
                   ON CONFLICT (text) DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.text, data.lessons_on_day_id])
}

async function insertTextAnswer(data) {
    const query = `INSERT
                   INTO text_answers(answer, question_id, student_id, lessons_on_day_id)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT (id) DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.answer, data.question_id, data.student_id, data.lessons_on_day_id])
}

async function insertNumericAnswer(data) {
    const query = `INSERT
                   INTO numeric_answers(answer, question_id, student_id, lessons_on_day_id)
                   VALUES ($1, $2, $3, $4)
                   ON CONFLICT (id) DO NOTHING
                   RETURNING *;`
    return await pgConnection.query(query, [data.answer, data.question_id, data.student_id, data.lessons_on_day_id])
}


async function selectAllAnswersOnLesson(data) {
    const query = `SELECT json_build_object(
                                  'question', q.text,
                                  'answers', (SELECT to_jsonb(array_agg(t))
                                              FROM ((SELECT answer, login
                                                     FROM (SELECT answer::text, question_id, student_id
                                                           FROM numeric_answers
                                                           WHERE (lessons_on_day_id = $1 AND question_id = q.id)
                                                           UNION
                                                           SELECT answer, question_id, student_id
                                                           FROM text_answers
                                                           WHERE (lessons_on_day_id = $1 AND question_id = q.id)) as ans
                                                              LEFT JOIN students ON ans.student_id = students.id)) as t
                                  )
                              ) AS answer
                   FROM questions as q
                   WHERE lessons_on_day_id = $1
                      OR lessons_on_day_id IS NULL;`
    return await pgConnection.query(query, [data.lessons_on_day_id])
}

async function selectAllQuestionsOnLesson(data) {
    const query = `SELECT json_build_object(
                                  'text', q.text,
                                  'id', q.id,
                                  'type', q.type
                              ) AS question
                   FROM questions as q
                   WHERE lessons_on_day_id = $1
                      OR lessons_on_day_id IS NULL;`
    return await pgConnection.query(query, [data.lessons_on_day_id])
}

async function selectUsersForNotificationAfterLesson() {
    const query = `SELECT *
                   FROM students
                            JOIN groups g on g.id = students.group_id
                            JOIN groups_on_lesson gol on students.group_id = gol.group_id
                            JOIN lessons_on_day lod on lod.id = gol.lessons_on_day_id
                            JOIN lessons l on lod.lesson_id = l.id
                            JOIN teachers_on_lesson tol on lod.id = tol.lessons_on_day_id
                            JOIN teachers t on tol.teacher_id = t.id
                   WHERE lod.time_end >= now()::time - INTERVAL '30 min'
                     AND lod.time_end < now()::time
                     AND students.telegram_id IS NOT NULL
                     AND t.telegram_id IS NOT NULL;`
    return await pgConnection.query(query, [])
}

async function selectTeachersForNotificationAfterEndQuiz() {
    const query = `SELECT *
                   FROM teachers
                            JOIN teachers_on_lesson tol on teachers.id = tol.teacher_id
                            JOIN lessons_on_day lod on lod.id = tol.lessons_on_day_id
                            JOIN lessons l on lod.lesson_id = l.id
                   WHERE lod.time_end >= now()::time - INTERVAL '2 day' 
                     AND lod.time_end < now()::time - INTERVAL '2 day' + INTERVAL '30 min'
                     AND teachers.telegram_id IS NOT NULL;`
    return await pgConnection.query(query, [])
}



