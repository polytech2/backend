module.exports = {
    query: query
};

const config = {
    host: 'localhost',
    max: 20,
    idleTimeoutMillis: 1000,
    connectionTimeoutMillis: 1000,
    user: 'postgresdocker',
    database: 'database',
    password: 'postgresdocker',
    port: 5432
};

//get the pg object
const pg = require('pg');

//object which holds the connection to the db
const pool = new pg.Pool(config);


/**
 * executes the specified sql query and provides a callback which is given
 * with the results in a DataResponseObject
 *
 * @param queryString
 * @param fields
 * @param callback - takes a DataResponseObject
 */
function query(queryString, fields, callback) {
    // console.log('start');
    return new Promise(resolve => {
        pool.connect(function (error, client, done) {
            if (error) {
                console.log('error on connect');
                resolve([error, null]);
            } else {
                client.query(queryString, fields, function (error, results) {
                    //call `done()` to release the client back to the pool
                    done();
                    // console.log('query: ', queryString);
                    // console.log('fields: ', fields);
                    // console.log('results: ', results);
                    if (error) {
                        console.log(error);
                        resolve([error, null]);
                    } else {
                        // console.log('norm');
                        resolve([error, results ? results.rows : null]);
                    }
                });
            }
        });
    });

}
