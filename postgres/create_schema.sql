--
-- PostgreSQL database dump
--

create schema if not exists public;
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;
create EXTENSION IF NOT EXISTS plpgsql with SCHEMA pg_catalog;
SET default_tablespace = '';

SET default_with_oids = false;

DO
$$
    BEGIN
        IF NOT EXISTS(SELECT * FROM pg_type WHERE typname = 'question_type') THEN
            create type QUESTION_TYPE as ENUM ('numeric', 'text');
        END IF;
    END
$$;

create table IF NOT exists faculties
(
    id   serial not null primary key,
    name text unique,
    abbr text
);

create table IF NOT exists groups
(
    id         serial not null primary key,
    name       text   not null unique,
    spec       text,
    faculty_id INT    NOT NULL REFERENCES faculties (id) ON DELETE CASCADE
);

create table IF NOT exists days
(
    id   serial not null primary key,
    date date   not null unique
);


create table IF NOT exists students
(
    id          serial not null primary key,
    telegram_id INT    NOT NULL unique,
    login       text,
    group_id    INT    NOT NULL REFERENCES groups (id) ON DELETE CASCADE
);

create table IF NOT exists teachers
(
    id          serial not null primary key,
    full_name   text   not null,
    chair       text,
    telegram_id INT unique
);

create table IF NOT exists lessons
(
    id            serial not null primary key,
    subject       text   not null,
    subject_short text   not null,
    type          int2,
    unique (subject, subject_short, type)
);

create table IF NOT exists lessons_on_day
(
    id         serial not null primary key,
    lesson_id  INT    NOT NULL REFERENCES lessons (id) ON DELETE CASCADE,
    day_id     INT    NOT NULL REFERENCES days (id) ON DELETE CASCADE,
    time_start time   not null,
    time_end   time   not null,
    unique (lesson_id, day_id, time_start, time_end)
);

create table IF NOT exists groups_on_lesson
(
    lessons_on_day_id INT NOT NULL REFERENCES lessons_on_day (id) ON DELETE CASCADE,
    group_id          INT NOT NULL REFERENCES groups (id) ON DELETE CASCADE,
    PRIMARY KEY (lessons_on_day_id, group_id)
);

create table IF NOT exists teachers_on_lesson
(
    lessons_on_day_id INT NOT NULL REFERENCES lessons_on_day (id) ON DELETE CASCADE,
    teacher_id        INT NOT NULL REFERENCES teachers (id) ON DELETE CASCADE,
    PRIMARY KEY (lessons_on_day_id, teacher_id)
);


create table IF NOT exists selected_lessons_for_teachers
(
    id         serial not null primary key,
    teacher_id INT    not null REFERENCES teachers (id) ON DELETE CASCADE,
    lesson_id  INT    not null REFERENCES lessons (id) ON DELETE CASCADE,
    group_id   INT    not null REFERENCES groups (id) ON DELETE CASCADE,
    unique (teacher_id, lesson_id, group_id)
);

create table IF NOT exists questions
(
    id                serial        not null primary key,
    text              text          not null unique,
    type              QUESTION_TYPE not null,
    lessons_on_day_id INT REFERENCES lessons_on_day (id) ON DELETE CASCADE --if null then default question
);

create table IF NOT exists text_answers
(
    id                serial not null primary key,
    answer            text   not null,
    lessons_on_day_id INT    not null REFERENCES lessons_on_day (id) ON DELETE CASCADE,
    question_id       INT    not null REFERENCES questions (id) ON DELETE CASCADE,
    student_id        INT REFERENCES students (id) ON DELETE CASCADE
);

create table IF NOT exists numeric_answers
(
    id                serial not null primary key,
    answer            int2   not null check ( answer BETWEEN 0 AND 10),
    lessons_on_day_id INT    not null REFERENCES lessons_on_day (id) ON DELETE CASCADE,
    question_id       INT    not null REFERENCES questions (id) ON DELETE CASCADE,
    student_id        INT REFERENCES students (id) ON DELETE CASCADE
);

INSERT INTO questions(text, type)
VALUES ('Насколько все было понятно? (0-10)', 'numeric'),
       ('Что было не понятно?', 'text'),
       ('Что хотелось бы услышать нового?', 'text')
ON CONFLICT DO NOTHING;
