let polytechCacher
let polytechNotificationManager
module.exports = (polytech_table_cacher, polytech_notification_manager) => {
    polytechCacher = polytech_table_cacher
    polytechNotificationManager = polytech_notification_manager
    return {
        init: initCron
    }
}


function initCron() {
    console.log('\x1b[36m%s\x1b[0m', 'Init Cron start...');

    const cron = require('node-cron');
    // run once in day in 02:00 update table
    cron.schedule('0 2 * * *', () => {
        console.log('\x1b[34m%s\x1b[0m', 'Cron update table start...');
        polytechCacher.download(() => {
                console.log('\x1b[34m%s\x1b[0m', 'Cron finish');
            }
        )
    });

    // run double in hours from 08:00 until 23:00 from Monday to Saturday
    cron.schedule('15,45 8-23 * * 1-6', () => {
        console.log('\x1b[34m%s\x1b[0m', 'Cron notification users after lesson start...');
        polytechNotificationManager.notificationUsersAfterLesson((result_list) => {
            //TODO send result to our front server
            console.log('\x1b[34m%s\x1b[0m', result_list);
            console.log('\x1b[34m%s\x1b[0m', 'Cron finish');
        })
    });

    // run double in hours every hour
    cron.schedule('0,30 * * * *', () => {
        console.log('\x1b[34m%s\x1b[0m', 'Cron notification teacher on end quiz start...');
        polytechNotificationManager.notificationTeacherOnEndQuiz((result_list) => {
            //TODO send result to our front server
            console.log('\x1b[34m%s\x1b[0m', result_list);
            console.log('\x1b[34m%s\x1b[0m', 'Cron finish');
        })
    });

    // run once in week in 01:00 for delete old rows
    cron.schedule('0 1 * * *', () => {
        console.log('\x1b[34m%s\x1b[0m', 'Cron delete old table start...');
        polytechCacher.clear(() => {
            console.log('\x1b[34m%s\x1b[0m', 'Cron finish');
        })
    });

    console.log('\x1b[36m%s\x1b[0m', 'Init Cron:success');
}
