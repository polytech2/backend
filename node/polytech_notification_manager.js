let tableDB
let quizDB
module.exports = (table_database, quiz_database) => {
    tableDB = table_database;
    quizDB = quiz_database;
    return {
        notificationUsersAfterLesson: notificationUsers,
        notificationTeacherOnEndQuiz: notificationTeacher,
    }
}

async function notificationUsers(callback) {
    const result = quizDB.getUsersForNotificationAfterLesson()
    callback(result)
}

async function notificationTeacher(callback) {
    const result = quizDB.getTeachersForNotificationAfterEndQuiz()
    callback(result)
}