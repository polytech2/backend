let db
module.exports = (database) => {
    db = database;
    return {
        download: downloadTables,
        clear: clearOldData,
    }
}

const url_polytech = `http://ruz2.spbstu.ru/api/v1/ruz/faculties/`
const url_scheduler = `http://ruz2.spbstu.ru/api/v1/ruz/scheduler/`
const isIterable = object =>
    object !== undefined && object != null && typeof object[Symbol.iterator] === 'function'

async function downloadTables(callback, onlyOneTable = false) {
    let counter = 0
    let response_faculty = await fetch(url_polytech);
    let data_faculty
    try {
        data_faculty = (await response_faculty.json()).faculties
    } catch (e) {
        console.log(e)
        return;
    }
    if (!isIterable(data_faculty)) return;
    for (const faculty of data_faculty) {
        let [error, _] = await db.addFaculty(faculty)
        if (error == null) {
            counter++
            await getGroups(faculty, (percent) => {
                console.log('FACULTY: ', counter + '/' + data_faculty.length, " GROUPS: ", percent);
            })
        }

        if (onlyOneTable) break;
    }
    console.log("COMPLETED");
    callback()
}

async function getGroups(faculty, callback) {
    let group_url = url_polytech + faculty.id + '/groups'
    let response_group = await fetch(group_url);
    let data_group
    try {
        data_group = (await response_group.json()).groups
    } catch (e) {
        console.log(e)
        return;
    }
    let counter = 0
    if (!isIterable(data_group)) return;
    for (const group of data_group) {
        group.faculty_id = faculty.id
        let [error, _] = await db.addGroup(group)
        if (error == null) {
            await getLessons(group, () => {
                counter++
                callback(counter + '/' + data_group.length);
            })
        }
    }
}

async function getLessons(group, callback) {
    let days_url = url_scheduler + group.id
    let response_days = await fetch(days_url);
    let data_days
    try {
        data_days = (await response_days.json()).days
    } catch (e) {
        console.log(e)
        return;
    }
    if (!isIterable(data_days)) return;
    for (const day of data_days) {
        let [error, day_db] = await db.addDay(day)
        if (error == null) {
            let list_lessons = day.lessons
            if (!isIterable(list_lessons)) continue;
            for (const lesson of list_lessons) {
                //add lesson on db
                let [l_error, les_rows] = await db.addLesson(lesson)
                if (l_error == null && les_rows.length > 0) {
                    let les = les_rows[0]
                    les.day_id = day_db[0].id;
                    les.lesson_id = les.id
                    les.time_start = lesson.time_start
                    les.time_end = lesson.time_end
                    let [e_addLessonsOnDay, res_addLessonsOnDay] = await db.addLessonsOnDay(les)

                    //add group on lesson
                    if (e_addLessonsOnDay == null && res_addLessonsOnDay.length > 0) {
                        let data = {
                            lessons_on_day_id: res_addLessonsOnDay[0].id,
                            group_id: group.id
                        }
                        await db.addGroupsOnLesson(data)
                    }

                    //add teacher in db
                    let list_teachers = lesson.teachers
                    if (!isIterable(list_teachers)) continue;
                    for (const teacher of list_teachers) {
                        await db.addTeacher(teacher)
                        let data = {
                            lessons_on_day_id: res_addLessonsOnDay[0].id,
                            teacher_id: teacher.id
                        }
                        await db.addTeacherOnLesson(data)
                    }
                }
            }
        }
    }
    setTimeout(() => {
    }, 100)
    callback()
}

async function clearOldData(callback) {
    await db.clearOldDays()
    callback()
}